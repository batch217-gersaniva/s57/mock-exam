let collection = [];
let head = 0;
let tail = 0;

// Write the queue functions below.

function print(){

    return collection;

}

function enqueue(item){
   
    collection.push(item);
    return [item];
}


function dequeue(){

if(isEmpty())
        return "Underflow";
    return collection.shift();
}



function front(){

    if(isEmpty())
            return "No elements in Queue";
    return collection[0];
}

function size(){
    return collection.length;
}

function isEmpty(){

    return collection.length == 0;

}








// Export create queue functions below.
module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};